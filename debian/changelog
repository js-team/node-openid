node-openid (2.0.10+~2.0.2-3) unstable; urgency=medium

  * Team upload
  * Update lintian overrides
  * Add trace-warnings in test to see from where come random failures

 -- Yadd <yadd@debian.org>  Wed, 23 Mar 2022 15:37:00 +0100

node-openid (2.0.10+~2.0.2-2) unstable; urgency=medium

  * Team upload
  * Render test optional (due to nodejs bug with i386 arch)

 -- Yadd <yadd@debian.org>  Wed, 15 Dec 2021 22:06:52 +0100

node-openid (2.0.10+~2.0.2-1) unstable; urgency=medium

  * Team upload
  * Drop autopkgtest for i386 due to nodejs bug
  * Embed typescript definitions and repack

 -- Yadd <yadd@debian.org>  Sat, 20 Nov 2021 09:26:00 +0100

node-openid (2.0.10-1) unstable; urgency=medium

  * Team upload
  * Fix GitHub tags regex
  * Update standards version to 4.6.0, no changes needed.
  * Fix filenamemangle
  * New upstream version 2.0.10
  * Add dependency to node-qs
  * Drop axios patch (now included in upstream)
  * Update test
  * Update lintian overrides

 -- Yadd <yadd@debian.org>  Mon, 08 Nov 2021 17:47:25 +0100

node-openid (2.0.7-1) unstable; urgency=medium

  * Team upload

  [ Debian Janitor ]
  * Use secure copyright file specification URI.
  * Bump debhelper from deprecated 8 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Change priority extra to priority optional.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Remove unnecessary XS-Testsuite field in debian/control.
  * Update Vcs-* headers to use salsa repository.
  * Apply multi-arch hints.
    + node-openid: Add Multi-Arch: foreign.

  [ Xavier Guimard ]
  * Bump debhelper compatibility level to 13
  * Declare compliance with policy 4.5.1
  * Add "Rules-Requires-Root: no"
  * Change section to javascript
  * Add debian/gbp.conf
  * Modernize debian/watch
  * Use dh-sequence-nodejs auto install (Closes: #974278)
  * New upstream version 2.0.7
  * Add lintian overrides
  * Add patch to replace request by axios
  * Enable upstream test using jest
  * Install sample.js example

 -- Xavier Guimard <yadd@debian.org>  Sat, 28 Nov 2020 12:41:04 +0100

node-openid (0.5.9-1) unstable; urgency=low

  * Initial release (Closes: #759507)

 -- Tim Retout <diocles@debian.org>  Wed, 27 Aug 2014 21:43:58 +0100
